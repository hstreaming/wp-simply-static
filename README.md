# Simply-static with S3 support
The plugin is an extension of [WordPress Simply Static plugin](http://simplystatic.co/). 

It provides next extra features:

1. **AWS S3** bucket upload
2. **AWS CloudFront** cache invalidation.
3. **Rest API** triggering endpoint

# Building the pluging
Best practices, how the plugins has to be structured and more check on [WordPress Developer Page](https://developer.wordpress.org/plugins/plugin-basics/best-practices/)

In order to get an installable plugin the next command has to be executed in the root of this repo

```git archive -o build/simply-static.zip --prefix=simply-static/ HEAD```

The command generates plugin package at `build/simply-static.zip`.

# Reference Infrastructure
![Overview](docs/overview.png)

## S3 Bucket
S3 allows static web site hosting. To get it up:

1. Create s3 bucket
2. Enable static website hosting as *Use this bucket to host a website* with index document set to `index.html` and error set to `404/index.html`
3. Enable public read access via Bucket policy

TF example:
```
resource "aws_s3_bucket" "static-bucket" {
  bucket        = "bucket-name"
  region        = "eu-central-1"
  force_destroy = "true"
  tags          = ""
  acl           = "public-read"

  
  website {
    index_document = "index.html"
    error_document = "404/index.html"
  }

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadForGetBucketObjects",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::bucket-name/*"
        }
    ]
}
EOF

}
```


# Certificate
If SSL is required for CloudFront distribution the certificate should exist in *us-east-1* AWS region for CloudFront alias(es) (host names where the static web site should be available from e.g. mycoolsite.com)

Create Certificate for the configured Aliases and refer it in CloudFront SSL Certificate section.

Here is an example how to refer an existing cert in TF:
```
data "aws_acm_certificate" "static_cert" {
  provider    = "aws.global"
  domain      = "WWW.ZONENAME.COM"
  types       = ["AMAZON_ISSUED"]
  statuses    = ["ISSUED"]
  most_recent = true
}
```

## CloudFront
CloudFront serves SSL and caching proxy to the S3 bucket.

Follow the default create procedure and specify *Origin Domain Name* to one from created S3 web hosting (e.g. adello-com.s3-website.eu-central-1.amazonaws.com)

*Important*: Do not reference S3 bucket as a resource or regional domain and instead point to a website domain name of the S3 bucket.

TF example:
```
resource "aws_cloudfront_distribution" "static_website" {
  enabled             = true
  comment             = "My static web site"
  default_root_object = ""
  price_class         = "PriceClass_200"
  aliases             = ["WWW.ZONENAME.COM"]
  tags                = "{tags=define}"

  origin {
    domain_name = "${aws_s3_bucket.website.website_domain }"
    origin_id   = "S3-${aws_s3_bucket.website.bucket}"
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "S3-${aws_s3_bucket.website.bucket}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "all"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    compress               = true
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  # This must be specified even if it's not used
  restrictions {
    geo_restriction {
      restriction_type = "none"
      locations        = []
    }
  }

  viewer_certificate {
    acm_certificate_arn = "${data.aws_acm_certificate.static_cert.arn}"
    ssl_support_method  = "sni-only"
  }
}

```

## DNS 
By default CloudFront registers only its own domain name. The other addresses from alias section have to be configured separately to point towards the default CloudFront host name.

This can be achived by creating a CNAME record in the DNS zone with a target pointing to CloudFront distribution (e.g. d3k9jrphaurfnv.cloudfront.net). 
However it is not allowed  for `@` (top) record be CNAME'd. The limitation has a workaround called Alias record In Route53.  

TF example:
```
data "aws_route53_zone" "default" {
  name = "ZONENAME.COM"
}

resource "aws_route53_record" "static_website_cdn" {
  name    = "WWW.ZONENAME.COM"
  zone_id = "${data.aws_route53_zone.default.zone_id}"
  type    = "A"

  alias {
    name                   = "${aws_cloudfront_distribution.static_website.domain_name}"
    zone_id                = "${aws_cloudfront_distribution.static_website.hosted_zone_id}"
    evaluate_target_health = false
  }
}
```

## IAM User
The plugin requires IAM user's *AWS Access key* and *AWS secret key* with the next policy attached to it
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "ListObjects",
            "Effect": "Allow",
            "Action": ["s3:ListBucket"],
            "Resource": ["arn:aws:s3:::bucket-name"]
        },
        {
            “Sid”: “AllowObjectACL”,
            “Effect”: “Allow”,
            “Action”: “s3:PutObjectAcl”,
            “Resource”: [“arn:aws:s3:::bucket-name/*“]
        },
        {
            "Sid": "AllObjectActions",
            "Effect": "Allow",
            "Action": "s3:*Object",
            "Resource": ["arn:aws:s3:::bucket-name/*"]
        }
        {
            "Sid": "AllowCacheInvalidation",
            "Effect": "Allow",
            "Action": "cloudfront:CreateInvalidation",
            "Resource": "*"
        }
    ]
}
```

TF example:

```
resource "aws_iam_user" "static_website_deployment_user" {
  name = "static_website_deployment_user"
}

resource "aws_iam_access_key" "static_website_deployment_user_key" {
  user = "${aws_iam_user.static_website_deployment_user.name}"
}

resource "aws_iam_user_policy" "static_website_deployment_user_policy" {
  name = "static_website_deployment_user_policy"
  user = "${aws_iam_user.static_website_deployment_user.name}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "ListObjects",
            "Effect": "Allow",
            "Action": ["s3:ListBucket"],
            "Resource": ["arn:aws:s3:::bucket-name"]
        },
        {
            "Sid": "AllObjectActions",
            "Effect": "Allow",
            "Action": "s3:*Object",
            "Resource": ["arn:aws:s3:::bucket-name/*"]
        },
        {
            "Sid": "AllowCacheInvalidation",
            "Effect": "Allow",
            "Action": "cloudfront:CreateInvalidation",
            "Resource": "*"
        }
    ]
}
EOF
}
```

# Licence
The Simply Static plugin is published under [GNU GPL v2 license](https://plugins.svn.wordpress.org/simply-static/trunk/license.txt) 
and so this repository inherits the original [license](license.txt). 
