<?php
namespace Simply_Static;

require_once(__DIR__.'/../libraries/aws/aws-autoloader.php');
require_once(__DIR__.'/../libraries/MimeTypes.php');


class Transfer_To_S3_Task extends Task {

	/**
	 * @var string
	 */
	protected static $task_name = 'transfer_to_s3';

	public function perform() {
	    if ($this->upload_to_s3()){
            $message = __( 'Transfer to S3 completed', 'simply-static' );
            $this->save_status_message( $message );
            return true;
        } else {
            $message = __( 'Transfer to S3 failed!', 'simply-static' );
            $this->save_status_message( $message );
            return false;
        }
	}

    public function upload_to_s3()
    {

        function UploadObject($S3, $Bucket, $Key, $Data, $ACL, $ContentType = "text/plain") {
            try {
                $Model = $S3->PutObject(array('Bucket'      => $Bucket,
                    'Key'         => $Key,
                    'Body'        => $Data,
                    'ACL'         => $ACL,
                    'ContentType' => $ContentType));
                return true;
            }
            catch (Exception $e) {
                $this->save_status_message( $e );
                throw new Exception($e);
            }
        }

        function UploadDirectory($S3, $Bucket, $dir, $siteroot) {
            $files = scandir($dir);
            foreach($files as $item){
                if($item != '.' && $item != '..'){
                    if(is_dir($dir.'/'.$item)) {
                        UploadDirectory($S3, $Bucket, $dir.'/'.$item, $siteroot);
                    } else if(is_file($dir.'/'.$item)) {
                        $clean_dir = str_replace($siteroot, '', $dir.'/'.$item);

                        $targetPath = $clean_dir;
                        $f = file_get_contents($dir.'/'.$item);

                        if($targetPath == '/index.html') {
                        }

                        UploadObject($S3, $Bucket, $targetPath, $f, 'public-read', GuessMimeType($item));
                    }
                }
            }
        }

        $archive_dir = $this->options->get_archive_dir();
        $access_key = $this->options->get( 'access_key' );
        $secret_key = $this->options->get( 'secret_key' );
        $bucket_name = $this->options->get( 'bucket_name' );
        $s3_region = $this->options->get( 's3_region' );

        $S3 = \Aws\S3\S3Client::factory(
            array(
                'version'=> '2006-03-01',
                'key'    => $access_key,
                'secret' => $secret_key,
                'region' => $s3_region,
            )
        );

        // Upload the directory to the bucket
        UploadDirectory($S3, $bucket_name, $archive_dir, $archive_dir.'/');

        //invalidate CloudFront
        $cloudfront_distribution = $this->options->get( 'cloudfront_distribution' );
        if(strlen($cloudfront_distribution)>=13) {
            $CloudFront = \Aws\CloudFront\CloudFrontClient::factory(
                array(
                    'version'		=> '2016-01-28',
                    'key'           => $access_key,
                    'secret'        => $secret_key,
                )
            );
            $cfresult = $CloudFront->createInvalidation(
                array(
                    'DistributionId' => $cloudfront_distribution,
                    'Paths' => array ( 'Quantity' => 1, 'Items' => array('/*')),
                    'CallerReference' => time()
                )
            );
        }
        return true;
    }
}
